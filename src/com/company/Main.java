package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // 1. Создать структуру данных для хранения информации о банке (название, курс доллара, лимит в гривне),
        // об обменнике (название, курс доллара, лимит в долларе), о чёрном рынке (одном чёрном рынке,
        // у которого также есть название и курс доллара, лимитов нет). Банк имеет лимит в 150 000 грн,
        // а обменник $20.000. Функционал - запросить у пользователя сумму в гривне и вывести в консоль кол-во валюты
        // после конвертации во всех организациях. Если обмен в организации не возможен - указать что операция не допустима.

        Organization[] organizations = Generator.Generate();

        System.out.println("Type the sum (in grivnas) that you want to change: ");
        Scanner scanner = new Scanner(System.in);
        float usersSum = Float.parseFloat(scanner.nextLine());

        //обмен только в обменке и самый лучший курс

        for (Organization organization : organizations) {
            if (organization.exchange(usersSum) > 0f) {
                System.out.println(String.format("in %s exchange is possible and we will get %.2f usd", organization.getName(), organization.exchange(usersSum)));
            }
        }

        System.out.println();
        System.out.println("Обмен только в обменках");
        for (Organization organization : organizations) {
            if (organization instanceof CurrencyExсhange) {
                System.out.println(String.format("In that %s currency exchange you will get %.2f usd", organization.getName(), organization.exchange(usersSum)));
            }
        }

        //самый лучший курс
        Organization best = organizations[0];
        for (int i = 1; i < organizations.length; i++) {
            if (best.usdBuyCourse < organizations[i].usdBuyCourse) {
                best = organizations[i];
            }
        }
        System.out.println(String.format("Orzanization with best course %s , you will get %.2f usd", best.getName(), best.exchange(usersSum)));

        //только банк
        System.out.println();
        System.out.println("ONLY BANKS");
        for (Organization organization : organizations) {
            if (organization instanceof Bank) {
                System.out.println(String.format("in bank: %s you will get %.2f", organization.getName(), organization.exchange(usersSum)));
            }
        }

        //only currency exchange
        System.out.println();
        System.out.println("ONLY CURRENCY EXCHANGE");
        for (Organization organization : organizations) {
            if (organization instanceof CurrencyExсhange) {
                System.out.println(String.format("in currency exchange: %s you will get %.2f usd", organization.getName(), organization.exchange(usersSum)));
            }
        }
    }

}
