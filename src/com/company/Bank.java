package com.company;

import java.util.Objects;

public class Bank extends Organization{
    private float uahLimit;
    private String currencyOfLimit;

    public Bank(String name, float usdBuyCourse, float uahLimit, String currencyOfLimit) {
        super(name, usdBuyCourse);
        this.uahLimit = uahLimit;
        this.currencyOfLimit = currencyOfLimit;
    }
    @Override
    public float exchange(float uah) {
        if (uah / usdBuyCourse < uahLimit){
            return super.exchange(uah);
        }else{
            return 0f;
        }
    }
}
