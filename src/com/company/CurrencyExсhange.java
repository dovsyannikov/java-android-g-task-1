package com.company;

public class CurrencyExсhange extends Organization {
    private String currencyOfLimit;
    private float limitValue;

    public CurrencyExсhange(String name, float usdBuyCourse, String currencyOfLimit, float limitValue) {
        super(name, usdBuyCourse);
        this.currencyOfLimit = currencyOfLimit;
        this.limitValue = limitValue;
    }

    @Override
    public float exchange(float uah) {
        float usd = super.exchange(uah);
        return usd < limitValue ? usd : 0f;
    }
}
