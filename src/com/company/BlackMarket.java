package com.company;

public class BlackMarket extends Organization {

    public BlackMarket(String name, float usdBuyCourse) {
        super(name, usdBuyCourse);
    }

    @Override
    public String toString() {
        return "BlackMarket{" +
                "name='" + name + '\'' +
                ", usdBuyCourse=" + usdBuyCourse +
                '}';
    }

    @Override
    public float exchange(float uah) {
        return super.exchange(uah);
    }
}
