package com.company;

public class Organization {

    public static final String USD = "usd";
    public static final String UAH = "uah";

    protected String name;
    protected float usdBuyCourse;

    public Organization(String name, float usdBuyCourse) {
        this.name = name;
        this.usdBuyCourse = usdBuyCourse;
    }

    public Organization() {
    }

    public String getName() {
        return name;
    }

    public float exchange(float uah) {
        return uah / usdBuyCourse;
    }


}
