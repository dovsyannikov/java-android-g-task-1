package com.company;

public class Generator {
    public static Organization[] Generate(){
            Organization [] organizations = new Organization[6];

            organizations[0] = new Bank("privat", 26.5f, 150_000f, Organization.UAH);
            organizations[1] = new Bank("pumb", 26.7f, 150_000f, Organization.UAH);
            organizations[2] = new Bank("oshad", 27.0f, 175_000f, Organization.UAH);
            organizations[3] = new CurrencyExсhange("57kharkov", 27.8f, Organization.USD, 20000);
            organizations[4] = new CurrencyExсhange("38kiev", 27.9f, Organization.USD, 25000);
            organizations[5] = new BlackMarket("1xbet", 25.9f);
            return organizations;
    }
}
